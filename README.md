# QCM Live - The multiple choice questionnaires management site

QCM Live is a web application which permits to manage multiple choice questionnaires live.
For example, this is useful in the classroom between a teacher and his students.

### Used technologies

* PHP
* HTML
* CSS
* Javascript

The model-view-controller design pattern is used.
