SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

-- STRUCTURE --

CREATE TABLE `groupe` (
  `id_grpe` int(11) NOT NULL,
  `num_grpe` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `groupe`
  ADD PRIMARY KEY (`id_grpe`),
  MODIFY `id_grpe` int(11) NOT NULL AUTO_INCREMENT;

CREATE TABLE `etudiant` (
  `id_etu` int(11) NOT NULL,
  `nom` text COLLATE utf8_bin NOT NULL,
  `prenom` text COLLATE utf8_bin NOT NULL,
  `email` text COLLATE utf8_bin NOT NULL,
  `login_etu` text COLLATE utf8_bin NOT NULL,
  `pass_etu` text COLLATE utf8_bin NOT NULL,
  `matricule` text COLLATE utf8_bin NOT NULL,
  `date_etu` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `etudiant`
  ADD PRIMARY KEY (`id_etu`),
  MODIFY `id_etu` int(11) NOT NULL AUTO_INCREMENT;
  
CREATE TABLE appartient (
  id_etu int(11) NOT NULL,
  id_grpe int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE appartient
  ADD PRIMARY KEY (id_etu, id_grpe),
  ADD CONSTRAINT FK_appartient_id_etu FOREIGN KEY(id_etu) REFERENCES etudiant(id_etu) ON DELETE CASCADE,
  ADD CONSTRAINT FK_appartient_id_grpe FOREIGN KEY(id_grpe) REFERENCES groupe(id_grpe) ON DELETE CASCADE;

CREATE TABLE `professeur` (
  `id_prof` int(11) NOT NULL,
  `nom` text COLLATE utf8_bin NOT NULL,
  `prenom` text COLLATE utf8_bin NOT NULL,
  `email` text COLLATE utf8_bin NOT NULL,
  `login_prof` text COLLATE utf8_bin NOT NULL,
  `pass_prof` text COLLATE utf8_bin NOT NULL,
  `date_prof` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `professeur`
  ADD PRIMARY KEY (`id_prof`),
  MODIFY `id_prof` int(11) NOT NULL AUTO_INCREMENT;

CREATE TABLE `theme` (
  `id_theme` int(11) NOT NULL,
  `titre_theme` text COLLATE utf8_bin NOT NULL,
  `desc_theme` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `theme`
  ADD PRIMARY KEY (`id_theme`),
  MODIFY `id_theme` int(11) NOT NULL AUTO_INCREMENT;

CREATE TABLE `question` (
  `id_quest` int(11) NOT NULL,
  `id_theme` int(11),
  `titre` text COLLATE utf8_bin NOT NULL,
  `texte` text COLLATE utf8_bin NOT NULL,
  `bmultiple` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `question`
  ADD PRIMARY KEY (`id_quest`),
  MODIFY `id_quest` int(11) NOT NULL AUTO_INCREMENT,
  ADD CONSTRAINT FK_question_id_theme FOREIGN KEY(id_theme) REFERENCES theme(id_theme) ON DELETE SET NULL;

CREATE TABLE `reponse` (
  `id_rep` int(11) NOT NULL,
  `id_quest` int(11),
  `texte_rep` text COLLATE utf8_bin NOT NULL,
  `bvalide` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `reponse`
  ADD PRIMARY KEY (`id_rep`),
  MODIFY `id_rep` int(11) NOT NULL AUTO_INCREMENT,
  ADD CONSTRAINT FK_reponse_id_quest FOREIGN KEY(id_quest) REFERENCES question(id_quest) ON DELETE SET NULL;

CREATE TABLE `test` (
  `id_test` int(11) NOT NULL,
  `id_prof` int(11),
  `id_grpe` int(11),
  `titre_test` text COLLATE utf8_bin NOT NULL,
  `date_test` date NOT NULL,
  `etat` TINYINT(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `test`
  ADD PRIMARY KEY (`id_test`),
  MODIFY `id_test` int(11) NOT NULL AUTO_INCREMENT,
  ADD CONSTRAINT FK_test_id_prof FOREIGN KEY(id_prof) REFERENCES professeur(id_prof) ON DELETE SET NULL,
  ADD CONSTRAINT FK_test_id_grpe FOREIGN KEY(id_grpe) REFERENCES groupe(id_grpe) ON DELETE SET NULL;

CREATE TABLE `qcm` (
  `id_test` int(11) NOT NULL,
  `id_quest` int(11) NOT NULL,
  `bAutorise` tinyint(1) NOT NULL,
  `bBloque` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `qcm`
  ADD PRIMARY KEY (`id_test`, `id_quest`),
  ADD CONSTRAINT FK_qcm_id_test FOREIGN KEY(id_test) REFERENCES test(id_test) ON DELETE CASCADE,
  ADD CONSTRAINT FK_qcm_id_quest FOREIGN KEY(id_quest) REFERENCES question(id_quest) ON DELETE CASCADE;

CREATE TABLE `resultat` (
  `id_test` int(11),
  `id_etu` int(11),
  `id_quest` int(11),
  `id_rep` int(11),
  `date_res` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `resultat`
  ADD PRIMARY KEY (id_test, id_etu, id_quest, id_rep),
  ADD CONSTRAINT FK_resultat_id_test FOREIGN KEY(id_test) REFERENCES test(id_test) ON DELETE CASCADE,
  ADD CONSTRAINT FK_resultat_id_etu FOREIGN KEY(id_etu) REFERENCES etudiant(id_etu) ON DELETE CASCADE,
  ADD CONSTRAINT FK_resultat_id_quest FOREIGN KEY(id_quest) REFERENCES question(id_quest) ON DELETE CASCADE,
  ADD CONSTRAINT FK_resultat_id_rep FOREIGN KEY(id_rep) REFERENCES reponse(id_rep) ON DELETE CASCADE;

CREATE TABLE `bilan` (
  `id_test` int(11),
  `id_etu` int(11),
  `note_test` float NOT NULL,
  `date_bilan` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `bilan`
  ADD PRIMARY KEY (id_test, id_etu),
  ADD CONSTRAINT FK_bilan_id_test FOREIGN KEY(id_test) REFERENCES test(id_test) ON DELETE CASCADE,
  ADD CONSTRAINT FK_bilan_id_etu FOREIGN KEY(id_etu) REFERENCES etudiant(id_etu) ON DELETE CASCADE;
  
-- DONNEES --

INSERT INTO `etudiant` (`id_etu`, `nom`, `prenom`, `email`, `login_etu`, `pass_etu`, `matricule`, `date_etu`) VALUES
(1, 'Durand', 'Killian', 'killian.durand@gmail.com', 'kdurand1', 'f8237d8959e03355010bb85cc3dc46a46fb31110', 'ii46592', '2017-10-07'),
(2, 'Delavoile', 'Mathieu', 'mathieu.delavoile@gmail.com', 'mdelav1', 'e0b2d81c80b94799aeef126f990f48cdb5505bbc', 'ii59115', '2017-10-07'),
(3, 'Foulon', 'Thomas', 'thomas.foulon@gmail.com', 'tfoulon1', 'a4ac914c09d7c097fe1f4f96b897e625b6922069', 'ii05460', '2017-10-25'),
(4, 'Boulanger', 'Valérie', 'valerie.boulanger@gmail.com', 'vboulan1', '7507d41ecbd162a0d6dfdaaa9988a91184351735', 'ii05405', '2017-10-25');

INSERT INTO `groupe` (`id_grpe`, `num_grpe`) VALUES
(1, 'gr201'),
(2, 'gr202'),
(3, 'ifetina2');

INSERT INTO `appartient` (`id_etu`, `id_grpe`) VALUES
(3, 1),
(4, 1),
(1, 2),
(2, 2),
(1, 3),
(2, 3),
(3, 3),
(4, 3);

INSERT INTO `professeur` (`id_prof`, `nom`, `prenom`, `email`, `login_prof`, `pass_prof`, `date_prof`) VALUES
(1, 'Karl', 'Yves', 'yves.karl@gmail.com', 'Karl', 'e0b2d81c80b94799aeef126f990f48cdb5505bbc', '2017-10-05'),
(2, 'Dupont', 'Jacques', 'jacques.dupont@yahoo.fr', 'Dupont', 'f8237d8959e03355010bb85cc3dc46a46fb31110', '2017-10-05');

INSERT INTO `theme` (`id_theme`, `titre_theme`, `desc_theme`) VALUES
(1, 'MVC', 'Structuration MVC pour les sites web.'),
(2, 'Encapsulation Java', 'QCM sur l\'encapsulation en Java');

INSERT INTO `question` (`id_quest`, `id_theme`, `titre`, `texte`, `bmultiple`) VALUES
(1, 1, 'Que signifie MVC ?', '', 0),
(2, 1, 'Que doit-on mettre dans le dossier C ?', '', 0),
(3, 1, 'Que doit-on mettre dans le dossier M ?', '', 0),
(4, 1, 'Que doit-on mettre dans le dossier V ?', '', 1),
(5, 1, 'Quel est ou quels sont les avantages de la structuration MVC ?', '', 1),
(6, 2, 'Qu\'initialise t\'on dans le constructeur d\'une classe ?', '', 0),
(7, 2, 'Quelle(s) classe(s) peut/peuvent avoir accès aux attributs déclarés privés d\'une classe ?', '', 0),
(8, 2, 'Qu\'est-ce que l\'encapsulation ?', '', 1);

INSERT INTO `reponse` (`id_rep`, `id_quest`, `texte_rep`, `bvalide`) VALUES
(1, 1, 'mysql, vue, contrôleur', 0),
(2, 1, 'modèle, valide, clic', 0),
(3, 1, 'modèle, vue, contrôleur', 1),
(4, 1, 'manipulation, vue, couverture', 0),
(5, 2, 'les fichiers templates', 0),
(6, 2, 'les fichiers d\'accès à la base de données', 0),
(7, 2, 'les fichiers permettant de lier les accès à la base et les vues', 1),
(8, 2, 'les images', 0),
(9, 3, 'les fichiers permettant de lier les accès à la base et les vues', 0),
(10, 3, 'les fichiers templates', 0),
(11, 3, 'les fichiers d\'accès à la base de données', 1),
(14, 3, 'les images', 0),
(15, 4, 'les fichiers permettant l\'accès à la base de données', 0),
(16, 4, 'les fichiers permettant de lier les accès à la base et les vues', 0),
(17, 4, 'les fichiers templates', 1),
(18, 4, 'les images', 1),
(19, 5, 'on ne gère plus les styles', 0),
(20, 5, 'on a une interface unifiée (index.php)', 1),
(21, 5, 'les accès à la base de données sont plus rapides', 0),
(22, 5, 'les services peuvent être conçus et maintenus séparément', 1),
(23, 5, 'les services sont clairement affichés dans l\'URL', 0),
(24, 6, 'les attributs statiques de cette classe', 0),
(25, 6, 'les méthodes de la classe', 0),
(26, 6, 'les attributs d\'une instance de cette classe', 1),
(27, 7, 'uniquement cette classe', 1),
(28, 7, 'les classes du même package', 0),
(29, 7, 'toutes les classes', 0),
(30, 8, 'créer des méthodes statiques uniquement', 0),
(31, 8, 'mettre les attributs de la classe en privé', 1),
(32, 8, 'créer une classe sans méthodes', 0),
(33, 8, 'cacher/protéger les attributs de cette classe', 1);

INSERT INTO `test` (`id_test`, `id_prof`, `id_grpe`, `titre_test`, `date_test`, `etat`) VALUES
(1, 1, 1, 'Test MVC gr201', '2017-10-08', 0),
(2, 1, 2, 'Test MVC gr202', '2017-10-11', 0),
(3, 2, 3, 'Encapsulation + MVC', '2017-10-15', 0),
(4, 1, 1, 'Encapsulation gr201', '2017-10-15', 0);

INSERT INTO `qcm` (`id_test`, `id_quest`, `bAutorise`, `bBloque`) VALUES
(1, 1, 0, 0),
(1, 2, 1, 0),
(1, 3, 1, 0),
(1, 4, 1, 0),
(1, 5, 0, 0),
(2, 1, 1, 0),
(2, 2, 0, 0),
(2, 3, 0, 0),
(2, 4, 0, 0),
(2, 5, 1, 0),
(3, 1, 1, 0),
(3, 2, 1, 0),
(3, 3, 1, 0),
(3, 4, 1, 0),
(3, 5, 1, 0),
(3, 6, 1, 0),
(3, 7, 1, 0),
(3, 8, 1, 0),
(4, 6, 0, 0),
(4, 7, 1, 0),
(4, 8, 1, 0);