<?php

function commencerTest(){
	header("Location: index.php?controle=infosQCM&action=afficherTest");
}

function repondreQ(){
	if(!isset($_GET['idQ'])){
		header("Location: index.php?controle=user&action=erreur");
		exit();
	}
	$nameTabRep = "repsQ" . $_GET['idQ'];
	$tabRep = $_POST[$nameTabRep];
	require("modele/etu/effectuerTest_DB.php");
	foreach($tabRep as $rep){
		$res = repondreQ_DB($_SESSION['QCM_actuel']['id_test'], $_SESSION['profil']['id_etu'], $_GET['idQ'], $rep);
		if(!$res){
			header("Location: index.php?controle=user&action=erreur");
			exit();
		}
	}
	header("Location: index.php?controle=infosQCM&action=afficherTest");
}

?>