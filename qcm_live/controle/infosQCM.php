<?php

function afficherTest(){
	if($_SESSION['QCM_actuel']['etat'] != '1'){
		header("Location: index.php?controle=user&action=erreur&erreur=8");
		exit();
	}
	require("modele/infosQCM_DB.php");
	if(isset($_SESSION['profil']['id_prof'])){
		// CAS PROFESSEUR
		// Récupération des questions
		$questions = getQuestionsProfQCM($_SESSION['QCM_actuel']['id_test']);
		// Page de gestion de test
		$page = "vue/infosQCM/afficherTestProf.tpl";
		// Récupération des réponses disponibles pour chaque question
		for($i = 0; $i < count($questions); ++$i){
			$questions[$i]['reponses'] = getReponses($questions[$i]['id_quest']);
			for($j = 0; $j < count($questions[$i]['reponses']); ++$j){
				// Récupération du nombre d'élèves ayant répondu cette réponse
				$questions[$i]['reponses'][$j]['nbRep'] = getNbReponsesEleves($_SESSION['QCM_actuel']['id_test'],
					$questions[$i]['id_quest'], $questions[$i]['reponses'][$j]['id_rep']);
			}
		}
	}
	else{
		// CAS ETUDIANT
		// Récupération des questions
		$questions = getQuestionsEtuQCM($_SESSION['QCM_actuel']['id_test']);
		// Page de réponses aux questions
		$page = "vue/infosQCM/afficherTestEtu.tpl";
		// Opérations sur les questions
		for($i = 0; $i < count($questions); ++$i){
			// L'étudiant a-t-il déjà répondu à la question ?
			$questions[$i]['repondue'] = isQuestionRepondue($_SESSION['QCM_actuel']['id_test'],
				$_SESSION['profil']['id_etu'], $questions[$i]['id_quest'])?1:0;
			// Récupération des réponses
			$questions[$i]['reponses'] = getReponses($questions[$i]['id_quest']);
			if($questions[$i]['repondue'] == 1){
				// Opérations sur les réponses
				for($j = 0; $j < count($questions[$i]['reponses']); ++$j){
					// La réponse a-t-elle été choisie ?
					$questions[$i]['reponses'][$j]['choisie'] = isReponseChoisie($_SESSION['QCM_actuel']['id_test'],
						$_SESSION['profil']['id_etu'], $questions[$i]['id_quest'],
						$questions[$i]['reponses'][$j]['id_rep']);
				}
			}
		}
	}
	// Récupération du groupe de la session de test
	$groupe = getGroupeQCM($_SESSION['QCM_actuel']['id_test']);
	// Récupération des thèmes du QCM
	$themes = getThemesQCM($_SESSION['QCM_actuel']['id_test']);
	
	require($page);
}

function choisirQCM(){
	require("modele/infosQCM_DB.php");
	if(count($_POST) > 0){
		// CAS FORMULAIRE REMPLI
		if(!isset($_GET['etat'])){
			header("Location: index.php?controle=user&action=erreur");
			exit();
		}
		$_SESSION['QCM_actuel'] = getQCM($_POST['qcmChoisi' . $_GET['etat']]);
		header("Location: index.php");
		exit();
	}
	$profil = $_SESSION['profil'];
	if(isset($profil['id_prof'])){
		// CAS PROFESSEUR
		$QCMs = getQCMsProf($_SESSION['profil']['id_prof']);
		if(count($QCMs) == 0){
			$_SESSION['messageAccueil'] = "Aucun QCM créé ... Créez en un dès maintenant !";
			header("Location: index.php");
			exit();
		}
	}
	else{
		// CAS ETUDIANT
		$QCMs = getQCMsEtu($_SESSION['profil']['id_etu']);
		if(count($QCMs) == 0){
			$_SESSION['messageAccueil'] = "Aucun QCM disponible ...";
			header("Location: index.php");
			exit();
		}
	}
	require("vue/infosQCM/choisirQCM.tpl");
}

function analyseResultats(){
	if($_SESSION['QCM_actuel']['etat'] != '2'){
		header("Location: index.php?controle=user&action=erreur&erreur=9");
		exit();
	}
	require("modele/infosQCM_DB.php");
	// CAS PROFESSEUR
	// Récupération des questions
	$questions = getQuestionsProfQCM($_SESSION['QCM_actuel']['id_test']);
	// Récupération des étudiants du groupe de test
	$etudiants = getEtudiantsTest($_SESSION['QCM_actuel']['id_test']);
	for($i = 0; $i < count($etudiants); ++$i){
		$etudiants[$i]['note_test'] = getNoteTestEtu($_SESSION['QCM_actuel']['id_test'],
			$etudiants[$i]['id_etu']);
	}
	// Page de gestion de test
	$page = "vue/infosQCM/analyseResultats.tpl";
	// Récupération des réponses disponibles pour chaque question
	for($i = 0; $i < count($questions); ++$i){
		$questions[$i]['reponses']['all'] = getReponses($questions[$i]['id_quest']);
		for($j = 0; $j < count($questions[$i]['reponses']['all']); ++$j){
			// Récupération du nombre d'étudiants ayant répondu cette réponse
			$questions[$i]['reponses']['all'][$j]['nbRep'] = getNbReponsesEleves($_SESSION['QCM_actuel']['id_test'],
				$questions[$i]['id_quest'], $questions[$i]['reponses']['all'][$j]['id_rep']);
			if(!isset($questions[$i]['reponses']['all'][$j]['nbRep'])) die('ok');
			// Récupération des réponses de chaque étudiants
			for($k = 0; $k < count($etudiants); ++$k){
				$nbRep = getNbReponsesEleve($_SESSION['QCM_actuel']['id_test'], $etudiants[$k]['id_etu']);
				if($nbRep == 0) $etudiants[$k]['absent'] = true;
				else $etudiants[$k]['absent'] = false;
				$etudiants[$k]['reponses'][$questions[$i]['id_quest']] = getIdReponsesEtu($etudiants[$k]['id_etu'],
					$_SESSION['QCM_actuel']['id_test'], $questions[$i]['id_quest']);
			}
		}
	}
	// Récupération du groupe de la session de test
	$groupe = getGroupeQCM($_SESSION['QCM_actuel']['id_test']);
	// Récupération des thèmes du QCM
	$themes = getThemesQCM($_SESSION['QCM_actuel']['id_test']);
	
	require($page);
}

?>