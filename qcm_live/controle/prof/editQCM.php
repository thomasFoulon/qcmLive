<?php
 
function creerQCM(){
	$id_groupe = isset($_POST['groupe'])?intval($_POST['groupe']):0;
	$titreTest = isset($_POST['titre'])?htmlspecialchars($_POST['titre']):'';
	$msg = '';
	
	require("modele/prof/editQCM_DB.php");
	$groupes = getGroupes();
	if(count($_POST) == 0){
		require("vue/prof/editQCM/creerQCM.tpl");
	}
	else if($titreTest == ''){
		$msg = 'Entrez un titre de QCM !';
		require("vue/prof/editQCM/creerQCM.tpl");
		exit();
	}
	else{
		$id_test = createTest($titreTest, intval($_SESSION['profil']['id_prof']), $id_groupe);
		if(!isset($id_test)){
			$msg = 'QCM existant ! Le couple Titre-Groupe existe déjà ...';
			require("vue/prof/editQCM/creerQCM.tpl");
			exit();
		}
		else{
			$_SESSION['messageAccueil'] = "Votre QCM a bien été créé !";
			header("Location: index.php");
			exit();
		}
	}
}

function afficherQCM($msg = ''){
	// RELIEE AU SERVICE modifQCM
	require_once("modele/prof/editQCM_DB.php");
	
	$questions = getQuestionsQCM($_SESSION['QCM_actuel']['id_test']);
	// Récupération des réponses disponibles pour chaque question
	for($i = 0; $i < count($questions); ++$i)
		$questions[$i]['reponses'] = getReponses($questions[$i]['id_quest']);
	// AFFICHAGE DU FORMULAIRE DE SELECTION DES QUESTIONS
	// Récupération des thèmes
	$themes = getThemes();
	require("vue/prof/editQCM/modifQCM.tpl");
}

function ajouterQuestionQCM(){
	// RELIEE AU SERVICE modifQCM
	$titre = isset($_POST['titre'])?htmlspecialchars($_POST['titre']):'';
	$reponses = isset($_POST['reponses'])?$_POST['reponses']:null;
	for($i = 0; $i < count($reponses); ++$i)
		$reponses[$i] = htmlspecialchars($reponses[$i]);
	$valides = isset($_POST['valides'])?htmlspecialchars($_POST['valides']):null;
	$theme = isset($_POST['theme'])?intval($_POST['theme']):0;
	$newTheme = isset($_POST['newTheme'])?htmlspecialchars($_POST['newTheme']):'';
	$msg = '';
	
	require("modele/prof/editQCM_DB.php");
	
	// Vérifications
	if($titre == '') $msg = "Entrez un titre de question !";
	else{
		if($theme == -1 and $newTheme == '') $msg = 'Entrez un nom de thème !';
		else foreach($reponses as $reponse)
			if($reponse == ''){ $msg = 'Vos réponses ne doivent pas être vides !'; break; }
		else if($valides == null) $msg = 'Au moins une réponse doit être correcte !';
	}
	
	if($msg != ''){
		afficherQCM($msg);
		exit();
	}
	
	if($theme == -1) $id_theme = ajouterTheme($newTheme);
	else $id_theme = $theme;
	$multiple = count($valides) >= 2 ? 1 : 0;
	$id_quest = ajouterQuestion($id_theme, $titre, $multiple);
	if($id_quest < 0){
		header("Location: index.php?controle=user&action=erreur");
		exit();
	}
	foreach($reponses as $num => $reponse){
		$valide = array_key_exists($num, $valides) ? '1' : '0';
		$res = ajouterReponse($id_quest, $reponse, $valide);
		if(!$res){
			header("Location: index.php?controle=user&action=erreur");
			exit();
		}
	}
	$res = lierQuestionQCM($_SESSION['QCM_actuel']['id_test'], $id_quest);
	if(!$res){
		header("Location: index.php?controle=user&action=erreur");
		exit();
	}
	$_SESSION['messageModif'] = "La question a bien été ajoutée !";
	header("Location: index.php?controle=prof/editQCM&action=modifQCM");
}

function modifQCM(){
	if(isset($_SESSION['messageModif'])){
		echo("<script text=\"text/javascript\">alert(\"". $_SESSION['messageModif'] ."\");</script>");
		unset($_SESSION['messageModif']);
	}
	
	if(isset($_GET['idQ'])){
		// Supprimer question (délier la question du qcm)
		require("modele/prof/editQCM_DB.php");
		$res = delierQuestionQCM($_SESSION['QCM_actuel']['id_test'], $_GET['idQ']);
		if(!$res) header("Location: index.php?controle=user&action=erreur");
		else header("Location: index.php?controle=prof/editQCM&action=modifQCM");
		exit();
	}
	
	if(count($_POST) == 0) afficherQCM();
	else ajouterQuestionQCM();
}

function supprimerQCM(){
	$id_test = $_SESSION['QCM_actuel']['id_test'];
	unset($_SESSION['QCM_actuel']);
	require("modele/prof/editQCM_DB.php");
	$res = supprimerQCM_DB($id_test);
	if(!$res)
		header("Location: index.php?controle=user&action=erreur");
	else{
		$_SESSION['messageAccueil'] = "Votre QCM a bien été supprimé !";
		header("Location: index.php");
	}
}

function setBilan(){
	// Service appelé lors de la terminaison du test
	if($_SESSION['QCM_actuel']['etat'] != '2'){
		header("Location: index.php?controle=user&action=erreur&erreur=9");
		exit();
	}
	require("modele/prof/editQCM_DB.php");
	$etudiants = getEtudiantsGroupe($_SESSION['QCM_actuel']['id_grpe']);
	$questions = getQuestionsQCM($_SESSION['QCM_actuel']['id_test']);
	$nbQuest = count($questions);
	for($i = 0; $i < $nbQuest; ++$i){
		$questions[$i]['reponses'] = getReponses($questions[$i]['id_quest']);
		$questions[$i]['nbRepValides'] = 0;
		foreach($questions[$i]['reponses'] as $reponse)
			if($reponse['bvalide'] == 1) ++$questions[$i]['nbRepValides'];
	}
	foreach($etudiants as $etudiant){
		// Bilan pour l'étudiant
		$note = 0;
		foreach($questions as $question){
			$resultats = getIdReponsesEtu($etudiant['id_etu'], $_SESSION['QCM_actuel']['id_test'],
				$question['id_quest']);
			foreach($question['reponses'] as $reponse){
				if($reponse['bvalide'] == 1){ // Gain de points
					if(in_array($reponse['id_rep'], $resultats))
						$note += (1 / $question['nbRepValides']);
				}
				else{ // Perte de points en cas de mauvaise réponse à une question à choix multiple
					if($question['bmultiple'] == 1 and in_array($reponse['id_rep'], $resultats)){
						$note -= (1 / $question['nbRepValides']);
					}
				}
			}
		}
		if($note < 0) $note = 0; // Pas de notes négatives
		$note = round($note * 20 / $nbQuest, 2);
		$res = setNote($_SESSION['QCM_actuel']['id_test'], $etudiant['id_etu'], $note);
		if(!$res){
			header("Location: index.php?controle=user&action=erreur");
			exit();
		}
	}
	header("Location: index.php");
}

?>