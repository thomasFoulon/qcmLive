<?php

function lancerTest(){
	require("modele/prof/gestionTest_DB.php");
	$nbQuest = getNbQuestions($_SESSION['QCM_actuel']['id_test']);
	if($nbQuest == 0){
		$_SESSION['messageAccueil'] = "Aucune question ne compose votre QCM ...";
		header("Location: index.php");
		exit();
	}
	$res = setEnCours($_SESSION['QCM_actuel']['id_test']);
	if(!$res){
		header("Location: index.php?controle=user&action=erreur");
		exit();
	}
	$_SESSION['QCM_actuel']['etat'] = 1;
	header("Location: index.php?controle=infosQCM&action=afficherTest");
}

function autoriserQ(){
	if(!isset($_GET['idQ'])){
		header("Location: index.php?controle=user&action=erreur");
		exit();
	}
	require("modele/prof/gestionTest_DB.php");
	$res = autoriserQuestion($_SESSION['QCM_actuel']['id_test'], intval($_GET['idQ']));
	if(!$res){
		header("Location: index.php?controle=user&action=erreur");
		exit();
	}
	header("Location: index.php?controle=infosQCM&action=afficherTest");
}

function bloquerQ(){
	if(!isset($_GET['idQ'])){
		header("Location: index.php?controle=user&action=erreur");
		exit();
	}
	require("modele/prof/gestionTest_DB.php");
	$res = bloquerQuestion($_SESSION['QCM_actuel']['id_test'], intval($_GET['idQ']));
	if(!$res){
		header("Location: index.php?controle=user&action=erreur");
		exit();
	}
	header("Location: index.php?controle=infosQCM&action=afficherTest");
}

function terminerTest(){
	require("modele/prof/gestionTest_DB.php");
	$res = setTerminé($_SESSION['QCM_actuel']['id_test']);
	if(!$res){
		header("Location: index.php?controle=user&action=erreur");
		exit();
	}
	$_SESSION['QCM_actuel']['etat'] = 2;
	// Bilan pour les étudiants
	header("Location: index.php?controle=prof/editQCM&action=setBilan");
}

 ?>