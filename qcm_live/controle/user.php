<?php

function accueil(){
	if(isset($_SESSION['profil'])){
		if(isset($_SESSION['profil']['id_prof']))
			require('vue/user/accueil/accueilProf.tpl');
		else require('vue/user/accueil/accueilEtu.tpl');
	}
	else require('vue/user/accueil/accueil.tpl');
	if(isset($_SESSION['messageAccueil'])){
		echo("<script text=\"text/javascript\">alert(\"". $_SESSION['messageAccueil'] ."\");</script>");
		unset($_SESSION['messageAccueil']);
	}
}

function ident($type="Etu"){
	if(isset($_SESSION['profil'])) header("Location: index.php");
	else{
		$login = isset($_POST['login'])?htmlspecialchars($_POST['login']):'';
		$password = isset($_POST['password'])?htmlspecialchars($_POST['password']):'';
		$msg = '';
		
		if(count($_POST) == 0)
			require("vue/user/ident/ident.tpl");
		else{
			$profil = array();
			require("modele/user_DB.php");
			if($type == "Etu")
				$res = verif_ident_etu($login, $password, $profil);
			else $res = verif_ident_prof($login, $password, $profil);
			if(!$res){
				$msg = "Erreur de saisie ...";
				require("vue/user/ident/ident.tpl");
			}
			else{
				$_SESSION['profil'] = $profil;
				$_SESSION['profil']['nom'] = utf8_encode($_SESSION['profil']['nom']);
				$_SESSION['profil']['prenom'] = utf8_encode($_SESSION['profil']['prenom']);
				header("Location: index.php");
			}
		}
	}
}

function identProf(){
	ident('Prof');
}

function identEtu(){
	ident('Etu');
}

function deconnect(){
	session_destroy();
	header("Location: index.php");
}

function erreur(){
	$erreur1 = 'Service inexistant ...';
	$erreur2 = 'Veuillez vous connecter pour accéder à ce service.';
	$erreur3 = 'Vous ne pouvez accéder à ce service qu\'en tant qu\'étudiant ...';
	$erreur4 = 'Vous ne pouvez accéder à ce service qu\'en tant que professeur ...';
	$erreur5 = 'Aucun QCM sélectionné ...';
	$erreur6 = 'Vous êtes déjà connecté ...';
	$erreur7 = 'Le QCM n\'est pas commencé ...';
	$erreur8 = 'Le QCM n\'est pas en cours ...';
	$erreur9 = 'Le QCM n\'est pas terminé ...';
	if(!isset($_GET['erreur']))
		$erreur = 'Quelque chose s\'est mal passé';
	else{
		$numErr = $_GET['erreur'];
		switch($numErr){
			case 1:
				$erreur = $erreur1;
				break;
			case 2:
				$erreur = $erreur2;
				break;
			case 3:
				$erreur = $erreur3;
				break;
			case 4:
				$erreur = $erreur4;
				break;
			case 5:
				$erreur = $erreur5;
				break;
			case 6:
				$erreur = $erreur6;
				break;
			case 7:
				$erreur = $erreur7;
				break;
			case 8:
				$erreur = $erreur8;
				break;
			case 9:
				$erreur = $erreur9;
				break;
			default:
				$erreur = $erreur1;
				break;
		}
	}
	require("vue/user/erreur.tpl");
}

?>