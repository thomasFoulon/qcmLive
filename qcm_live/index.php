<?php
	session_start();
	
	$serviceList = array(
		'user' => array(1 => 'accueil', 'ident', 'identEtu', 'identProf', 'deconnect', 'erreur'),
		'infosQCM' => array(1 => 'choisirQCM', 'afficherTest', 'analyseResultats'),
		'prof/editQCM' => array(1 => 'creerQCM', 'modifQCM', 'supprimerQCM', 'setBilan'),
		'prof/gestionTest' => array(1 => 'lancerTest', 'autoriserQ', 'bloquerQ', 'debloquerQ', 'terminerTest'),
		'etu/effectuerTest' => array(1 => 'commencerTest', 'repondreQ', 'terminerTest')
	);
	
	$currentQcmNeeded = array(
		'afficherTest', 'analyseResultats',
		'modifQCM', 'supprimerQCM', 'setBilan', 'prof/gestionTest',
		'etu/effectuerTest'
	);
	
	if(count($_GET) == 0) header("Location: index.php?controle=user&action=accueil");
	else{
		$controle = $_GET['controle'];
		$action = $_GET['action'];
		
		if(!array_key_exists($controle, $serviceList)){
			header("Location: index.php?controle=user&action=erreur&erreur=1");
		}
		if(!array_search($action, $serviceList[$controle])){
			header("Location: index.php?controle=user&action=erreur&erreur=1");
			exit();
		}
		
		if($controle != 'user' and !isset($_SESSION['profil'])){
			header("Location: index.php?controle=user&action=erreur&erreur=2");
			exit();
		}
		
		$user = explode("/", $controle)[0];
		
		if($user == 'etu' and !isset($_SESSION['profil']['id_etu'])){
			echo($user);
			header("Location: index.php?controle=user&action=erreur&erreur=3");
			exit();
		}
		
		if($user == 'prof' and !isset($_SESSION['profil']['id_prof'])){
			header("Location: index.php?controle=user&action=erreur&erreur=4");
			exit();
		}
		
		if(array_search($controle, $currentQcmNeeded) or array_search($action, $currentQcmNeeded))
			if(!isset($_SESSION['QCM_actuel'])){
				header("Location: index.php?controle=user&action=erreur&erreur=5");
				exit();
			}
		
		require("./controle/" . $controle . ".php");
		$action();
	}
?>