<?php

require("modele/connectDB.php");

function getQCM($id_test){
	$linkPDO = connect_pdo();
	$req = "SELECT * FROM test WHERE id_test = ?;";
	try {
		$prep = $linkPDO->prepare($req);
		$prep->execute(array($id_test));
		$res = $prep->fetchAll();
	}
	catch(Exception $e) { die("Echec : " . $e->getMessage()); }
	return count($res)>0?$res[0]:null;
}

function getQCMsProf($id_prof){
	$linkPDO = connect_pdo();
	$req = "SELECT * FROM test WHERE id_prof = ?;";
	try {
		$prep = $linkPDO->prepare($req);
		$prep->execute(array($id_prof));
		$res = $prep->fetchAll();
	}
	catch(Exception $e) { die("Echec : " . $e->getMessage()); }
	return count($res)>0?$res:null;
}

function getQCMsEtu($id_etu){
	$linkPDO = connect_pdo();
	$req = "SELECT * FROM test t, appartient a, etudiant e
			WHERE t.id_grpe = a.id_grpe
			AND a.id_etu = e.id_etu
			AND e.id_etu = ?
			AND t.etat <> '0'
			AND e.date_etu <= t.date_test
			ORDER BY a.id_grpe;";
	try {
		$prep = $linkPDO->prepare($req);
		$prep->execute(array($id_etu));
		$res = $prep->fetchAll();
	}
	catch(Exception $e) { die("Echec : " . $e->getMessage()); }
	return count($res)>0?$res:null;
}

function getQuestionsProfQCM($id_test){
	$linkPDO = connect_pdo();
	$req = "SELECT * FROM qcm q INNER JOIN question qu ON q.id_quest = qu.id_quest WHERE q.id_test = ?;";
	try {
		$prep = $linkPDO->prepare($req);
		$prep->execute(array($id_test));
		$res = $prep->fetchAll();
	}
	catch(Exception $e) { die("Echec : " . $e->getMessage()); }
	return count($res)>0?$res:null;
}

function getQuestionsEtuQCM($id_test){
	$linkPDO = connect_pdo();
	$req = "SELECT * FROM qcm q INNER JOIN question qu ON q.id_quest = qu.id_quest WHERE id_test = ? AND q.bAutorise = 1;";
	try {
		$prep = $linkPDO->prepare($req);
		$prep->execute(array($id_test));
		$res = $prep->fetchAll();
	}
	catch(Exception $e) { die("Echec : " . $e->getMessage()); }
	return count($res)>0?$res:null;
}

function getReponses($id_quest){
	$linkPDO = connect_pdo();
	$req = "SELECT * FROM reponse WHERE id_quest = ?;";
	try {
		$prep = $linkPDO->prepare($req);
		$prep->execute(array($id_quest));
		$res = $prep->fetchAll();
	}
	catch(Exception $e) { die("Echec : " . $e->getMessage()); }
	return count($res)>0?$res:null;
}

function getGroupeQCM($id_test){
	$linkPDO = connect_pdo();
	$req = "SELECT g.*, count(a.id_etu) AS nb_etu " .
			"FROM test t, appartient a, groupe g " .
			"WHERE t.id_test = ? " .
			"AND a.id_grpe = t.id_grpe " .
			"AND g.id_grpe = t.id_grpe;";
	try {
		$prep = $linkPDO->prepare($req);
		$prep->execute(array($id_test));
		$res = $prep->fetchAll();
	}
	catch(Exception $e) { die("Echec : " . $e->getMessage()); }
	return count($res)>0?$res[0]:null;
}

function getThemesQCM($id_test){
	$linkPDO = connect_pdo();
	$req = "SELECT DISTINCT t.* FROM theme t, question q, qcm a
			WHERE a.id_test = ?
			AND a.id_quest = q.id_quest
			AND q.id_theme = t.id_theme;";
	try {
		$prep = $linkPDO->prepare($req);
		$prep->execute(array($id_test));
		$res = $prep->fetchAll();
	}
	catch(Exception $e) { die("Echec : " . $e->getMessage()); }
	return count($res)>0?$res:null;
}

function isQuestionRepondue($id_test, $id_etu, $id_quest){
	$linkPDO = connect_pdo();
	$req = "SELECT * FROM resultat WHERE id_test = ? AND id_etu = ? AND id_quest = ?;";
	try {
		$prep = $linkPDO->prepare($req);
		$prep->execute(array($id_test, $id_etu, $id_quest));
		$res = $prep->fetchAll();
	}
	catch(Exception $e) { die("Echec : " . $e->getMessage()); }
	return count($res)>0;
}

function isReponseChoisie($id_test, $id_etu, $id_quest, $id_rep){
	$linkPDO = connect_pdo();
	$req = "SELECT * FROM resultat WHERE id_test = ? AND id_etu = ? AND id_quest = ? AND id_rep = ?;";
	try {
		$prep = $linkPDO->prepare($req);
		$prep->execute(array($id_test, $id_etu, $id_quest, $id_rep));
		$res = $prep->fetchAll();
	}
	catch(Exception $e) { die("Echec : " . $e->getMessage()); }
	return count($res)>0;
}

function getNbReponsesEleves($id_test, $id_quest, $id_rep){
	$linkPDO = connect_pdo();
	$req = "SELECT count(id_etu) AS nbRep FROM resultat WHERE id_test = ? AND id_quest = ? AND id_rep = ?;";
	try {
		$prep = $linkPDO->prepare($req);
		$prep->execute(array($id_test, $id_quest, $id_rep));
		$res = $prep->fetch();
	}
	catch(Exception $e) { die("Echec : " . $e->getMessage()); }
	return $res['nbRep'];
}

function getNbReponsesEleve($id_test, $id_etu){
	$linkPDO = connect_pdo();
	$req = "SELECT count(id_rep) AS nbRep FROM resultat WHERE id_test = ? AND id_etu = ?;";
	try {
		$prep = $linkPDO->prepare($req);
		$prep->execute(array($id_test, $id_etu));
		$res = $prep->fetch();
	}
	catch(Exception $e) { die("Echec : " . $e->getMessage()); }
	return $res['nbRep'];
}

function getEtudiantsTest($id_test){
	$linkPDO = connect_pdo();
	$req = "SELECT e.*
			FROM etudiant e, appartient a, test t
			WHERE a.id_etu = e.id_etu
			AND a.id_grpe = t.id_grpe
			AND t.id_test = ?
			AND e.date_etu <= t.date_test;";
	try{
		$prep = $linkPDO->prepare($req);
		$prep->execute(array($id_test));
		$res = $prep->fetchAll();
	}catch(Exception $e){ die("Echec : " . $e->getMessage()); }
	return count($res)>0?$res:null;
}

function getIdReponsesEtu($id_etu, $id_test, $id_quest){
	$linkPDO = connect_pdo();
	$req = "SELECT id_rep FROM resultat WHERE id_etu = ? AND id_test = ? AND id_quest = ?;";
	try {
		$prep = $linkPDO->prepare($req);
		$prep->execute(array($id_etu, $id_test, $id_quest));
		while($res[] = $prep->fetch()['id_rep']){}
	}
	catch(Exception $e) { die("Echec : " . $e->getMessage()); }
	return count($res)>0?$res:null;
}

function getNoteTestEtu($id_test, $id_etu){
	$linkPDO = connect_pdo();
	$req = "SELECT note_test FROM bilan WHERE id_test = ? AND id_etu = ?";
	try {
		$prep = $linkPDO->prepare($req);
		$prep->execute(array($id_test, $id_etu));
		$res = $prep->fetchAll();
	}
	catch(Exception $e) { die("Echec : " . $e->getMessage()); }
	return count($res)>0?$res[0]['note_test']:null;
}

?>