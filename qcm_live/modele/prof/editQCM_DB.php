<?php

require("modele/connectDB.php");

function get($table){
	$linkPDO = connect_pdo();
	$req = "SELECT * FROM $table;";
	try {
		$prep = $linkPDO->prepare($req);
		$prep->execute();
		$res = $prep->fetchAll();
	}
	catch(Exception $e) { die("Echec : " . $e->getMessage()); }
	if(count($res) <= 0)
		return null;
	else return $res;
}

function getGroupes(){
	return get('groupe');
}

function getThemes(){
	return get('theme');
}

function getQuestionsQCM($id_test){
	$linkPDO = connect_pdo();
	$req = "SELECT * FROM qcm q INNER JOIN question qu ON q.id_quest = qu.id_quest WHERE id_test = ?;";
	try {
		$prep = $linkPDO->prepare($req);
		$prep->execute(array($id_test));
		$res = $prep->fetchAll();
	}
	catch(Exception $e) { die("Echec : " . $e->getMessage()); }
	return count($res)>0?$res:null;
}

function isTest($titreTest, $id_prof, $id_groupe){
	$linkPDO = connect_pdo();
	$req = "SELECT * FROM test WHERE id_prof = ? AND id_grpe = ? AND titre_test = ?;";
	try {
		$prep = $linkPDO->prepare($req);
		$prep->execute(array($id_prof, $id_groupe, $titreTest));
		$res = $prep->fetchAll();
	}
	catch(Exception $e) { die("Echec : " . $e->getMessage()); }
	return count($res)>0?$res[0]:null;
}

function createTest($titreTest, $id_prof, $id_groupe){
	$linkPDO = connect_pdo();
	if(isTest($titreTest, $id_prof, $id_groupe) != null) return null;
	// CREATION A L'ETAT 0 -> Pas encore lanc�
	$req = "INSERT INTO test VALUES(NULL, ?, ?, ?, CURDATE(), 0);";
	try {
		$prep = $linkPDO->prepare($req);
		$prep->execute(array($id_prof, $id_groupe, $titreTest));
	}
	catch(Exception $e) { die("Echec : " . $e->getMessage()); }
	
	$test = isTest($titreTest, $id_prof, $id_groupe);
	return $test['id_test'];
}

function getQuestionsTheme($id_theme){
	$linkPDO = connect_pdo();
	$req = "SELECT * FROM question WHERE id_theme = ?;";
	try {
		$prep = $linkPDO->prepare($req);
		$prep->execute(array($id_theme));
		$res = $prep->fetchAll();
	}
	catch(Exception $e) { die("Echec : " . $e->getMessage()); }
	return count($res)>0?$res:null;
}

function ajoutThemeQuestTest($id_test, $id_theme){
	$linkPDO = connect_pdo();
	$questions = getQuestionsTheme($id_theme);
	if(!isset($questions)) return false;
	$req = "INSERT INTO qcm VALUES(?, ?, 0, 0);";
	foreach($questions as $question){
		try {
			$prep = $linkPDO->prepare($req);
			$prep->execute(array($id_test, $question['id_quest']));
		}
		catch(Exception $e) { return false; }
	}
	return true;
}

function supprimerQCM_DB($id_test){
	$linkPDO = connect_pdo();
	$req = "DELETE FROM test WHERE id_test = ?;";
	try {
		$prep = $linkPDO->prepare($req);
		$prep->execute(array($id_test));
	}
		catch(Exception $e) { return false; }
	return true;
}

function ajouterQuestion($id_theme, $titre, $multiple){
	$linkPDO = connect_pdo();
	$req = "INSERT INTO question VALUES(NULL, ?, ?, '', ?);";
	try {
		$prep = $linkPDO->prepare($req);
		$prep->execute(array($id_theme, $titre, $multiple));
	}
		catch(Exception $e) { return -1; }
	return $linkPDO->lastInsertId();
}

function ajouterTheme($titre_theme){
	$linkPDO = connect_pdo();
	$req = "INSERT INTO theme VALUES(NULL, ?, '');";
	try {
		$prep = $linkPDO->prepare($req);
		$prep->execute(array($titre_theme));
	}
		catch(Exception $e) { return -1; }
	return $linkPDO->lastInsertId();
}

function ajouterReponse($id_quest, $texte_rep, $valide){
	$linkPDO = connect_pdo();
	$req = "INSERT INTO reponse VALUES(NULL, ?, ?, ?);";
	try {
		$prep = $linkPDO->prepare($req);
		$prep->execute(array($id_quest, $texte_rep, $valide));
	}
		catch(Exception $e) { return false; }
	return true;
}

function lierQuestionQCM($id_test, $id_quest){
	$linkPDO = connect_pdo();
	$req = "INSERT INTO qcm VALUES(?, ?, 0, 0);";
	try {
		$prep = $linkPDO->prepare($req);
		$prep->execute(array($id_test, $id_quest));
	}
		catch(Exception $e) { return false; }
	return true;
}

function delierQuestionQCM($id_test, $id_quest){
	$linkPDO = connect_pdo();
	$req = "DELETE FROM qcm WHERE id_test = ? AND id_quest = ?;";
	try {
		$prep = $linkPDO->prepare($req);
		$prep->execute(array($id_test, $id_quest));
	}
		catch(Exception $e) { return false; }
	return true;
}

function getReponses($id_quest){
	$linkPDO = connect_pdo();
	$req = "SELECT * FROM reponse WHERE id_quest = ?;";
	try {
		$prep = $linkPDO->prepare($req);
		$prep->execute(array($id_quest));
		$res = $prep->fetchAll();
	}
	catch(Exception $e) { die("Echec : " . $e->getMessage()); }
	return count($res)>0?$res:null;
}

function getEtudiantsGroupe($id_grpe){
	$linkPDO = connect_pdo();
	$req = "SELECT e.* FROM appartient a, etudiant e WHERE e.id_etu = a.id_etu AND a.id_grpe = ?;";
	try{
		$prep = $linkPDO->prepare($req);
		$prep->execute(array($id_grpe));
		$res = $prep->fetchAll();
	}catch(Exception $e){ die("Echec : " . $e->getMessage()); }
	return count($res)>0?$res:null;
}

function getIdReponsesEtu($id_etu, $id_test, $id_quest){
	$linkPDO = connect_pdo();
	$req = "SELECT id_rep FROM resultat WHERE id_etu = ? AND id_test = ? AND id_quest = ?;";
	try {
		$prep = $linkPDO->prepare($req);
		$prep->execute(array($id_etu, $id_test, $id_quest));
		while($res[] = $prep->fetch()['id_rep']){}
	}
	catch(Exception $e) { die("Echec : " . $e->getMessage()); }
	return count($res)>0?$res:null;
}

function setNote($id_test, $id_etu, $note){
	$linkPDO = connect_pdo();
	$req = "INSERT INTO bilan VALUES(?, ?, ?, CURDATE());";
	try {
		$prep = $linkPDO->prepare($req);
		$prep->execute(array($id_test, $id_etu, $note));
	}
	catch(Exception $e) { return false; }
	return true;
}

?>