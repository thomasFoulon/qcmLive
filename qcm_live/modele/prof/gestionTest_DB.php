<?php

require("modele/connectDB.php");

function getNbQuestions($id_test){
	$linkPDO = connect_pdo();
	$req = "SELECT count(id_quest) AS nbQuest FROM qcm WHERE id_test = ?;";
	try {
		$prep = $linkPDO->prepare($req);
		$prep->execute(array($id_test));
		$res = $prep->fetch();
	}
	catch(Exception $e) { return false; }
	return isset($res)?$res['nbQuest']:0;
}

function setEnCours($id_test){
	$linkPDO = connect_pdo();
	$req = "UPDATE test SET etat = 1 WHERE id_test = ?;";
	try {
		$prep = $linkPDO->prepare($req);
		$prep->execute(array($id_test));
	}
	catch(Exception $e) { return false; }
	return true;
}

function setTerminé($id_test){
	$linkPDO = connect_pdo();
	$req = "UPDATE test SET etat = 2 WHERE id_test = ?;";
	try {
		$prep = $linkPDO->prepare($req);
		$prep->execute(array($id_test));
	}
	catch(Exception $e) { return false; }
	return true;
}

function autoriserQuestion($id_test, $id_question){
	$linkPDO = connect_pdo();
	$req = "UPDATE qcm SET bAutorise = 1 WHERE id_test = ? AND id_quest = ?";
	try {
		$prep = $linkPDO->prepare($req);
		$prep->execute(array($id_test, $id_question));
	}
	catch(Exception $e) { return false; }
	return true;
}

function bloquerQuestion($id_test, $id_question){
	$linkPDO = connect_pdo();
	$req = "UPDATE qcm SET bBloque = 1 WHERE id_test = ? AND id_quest = ?";
	try {
		$prep = $linkPDO->prepare($req);
		$prep->execute(array($id_test, $id_question));
	}
	catch(Exception $e) { return false; }
	return true;
}

?>