<?php

// Pour utiliser connect_pdo()
require("modele/connectDB.php");

function verif_ident($login, $password, &$profil, $req){
	$linkPDO = connect_pdo();
	$pass = sha1($password);
	try {
		$prep = $linkPDO->prepare($req);
		$prep->execute(array($login, $pass));
		$res = $prep->fetchAll();
	}
	catch(Exception $e) { die("Echec : " . $e->getMessage()); }
	if(count($res) > 0){
		$profil = $res[0];
		return true;
	}
	else return false;
}

function verif_ident_prof($login, $password, &$profil){
	$req = "SELECT * FROM professeur WHERE login_prof = ? AND pass_prof = ?;";
	return verif_ident($login, $password, $profil, $req);
}

function verif_ident_etu($login, $password, &$profil){
	$req = "SELECT * FROM etudiant WHERE login_etu = ? AND pass_etu = ?;";
	return verif_ident($login, $password, $profil, $req);
}

?>