<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8" />
    <title>QCM Live</title>
    <link rel="stylesheet" href="vue/stylesCSS/style.css" />
    <link rel="stylesheet" href="vue/stylesCSS/styleEtu.css" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <script src="vue/scriptJS/script.js"></script>
  </head>
  <body>
    <div id="header">
      <?php /* PROFIL */ if(isset($_SESSION['profil'])){ ?>
      <p class="profil">
      <u>Connecté en tant que</u> : 
      <b>
        <?php echo($_SESSION['profil']['prenom'] . " " . $_SESSION['profil']['nom']); ?>
      </b></p>
      <p class="profil">
        <a href="index.php?controle=user&action=deconnect">Se déconnecter</a>
      </p><?php } ?>
      <h1>QCM Live</h1>
      <p id="descr">Le site de gestion de QCM en temps réel !</p>
    </div>
    <div id="contenu">
	  <a id="accueil" href="index.php">Revenir à l'accueil</a>
      <div id="titreContenu">
        <h2>Session de test</h2>
        <h4>Test en cours : 
        <u>
          <?php echo($_SESSION['QCM_actuel']['titre_test']); ?>
        </u></h4>
      </div>
	  <div id="infos">
		<p><u>Groupe</u> : <?php echo $groupe['num_grpe']; ?>
		(<?php echo $groupe['nb_etu']?>)
		</p>
		<p><u>Thèmes</u> : 
		<?php
		for($i = 0; $i < count($themes); ++$i){
			if($i == count($themes) - 1)
				echo($themes[$i]['titre_theme'] . ".");
			else echo($themes[$i]['titre_theme'] . ", ");
		}
		?>
		</p>
	  </div>
      <div class="formulaire">
        <form action="index.php?controle=etu/effectuerTest&action=terminerTest" method="post">
		<?php
		if(!isset($questions))
			echo("<p id=msgErr>Aucune question autorisée par votre professeur pour le moment</p>");
		else{
			$compt = 0;
			/* DEBUT FOR EACH */
			foreach($questions as $question){
				++$compt;
				$choixM = $question['bmultiple']==1?'(CM)':'';
		?>
            <fieldset <?php if($question['bBloque'] == 1){ echo "class=\"bloque\""; }?> >
			  <legend>Question <?php echo($compt . " " . $choixM); ?></legend>
			  <!-- TITRE -->
			  <p class="titreQuestion"><?php echo((utf8_encode($question['titre']))); ?></p>
			  <?php if($question['bBloque'] == 0){ ?>
			  <!-- REPONSES -->
			  <div class="divReponses">
			  <?php foreach($question['reponses'] as $reponse){
						if($question['repondue'] == 0){
							$href = "href=\"javascript:void(0);\"";
							$onclick = "onclick=\"checkRep(this, ". $question['bmultiple'] . ");\"";
							$colorRep = "#FFAFAA"; // rouge
						}
						else{
							$href = '';
							$onclick = '';
							if($reponse['choisie'] == 1) $colorRep = "#949494"; // gris foncé
							else $colorRep = "#DEDEDE"; // gris clair
						}
			  ?>
			    <a class="reponseQ" <?php echo $href; ?> <?php echo $onclick; ?>>
					<p><span class="fa fa-check-circle-o fa-2x" style="color:<?php echo $colorRep; ?>"></span>
					<?php echo utf8_encode($reponse['texte_rep']); ?></p>
					<input type="checkbox" name="repsQ<?php echo $question['id_quest']; ?>[]" value="<?php echo $reponse['id_rep']; ?>">
				</a>
			  <?php } ?>
			  </div>
			  <br>
			  <?php if($question['repondue'] == 0){ ?>
			  <input type="submit" value="Valider"
			  formaction="index.php?controle=etu/effectuerTest&action=repondreQ&idQ=<?php echo $question['id_quest']; ?>">
			  <?php } } ?>
			</fieldset>
			<?php } } ?>
          <!-- FIN FOR EACH -->
        </form>
      </div>
    </div>
    <div id="footer">
      <p id="creators">Site by Thomas FOULON, Guillaume HEU &amp; Adam POUX</p>
      <img src="vue/stylesCSS/images/quiz.png" />
    </div>
  </body>
</html>
