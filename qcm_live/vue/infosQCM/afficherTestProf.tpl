<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8" />
    <title>QCM Live</title>
    <link rel="stylesheet" href="vue/stylesCSS/style.css" />
    <link rel="stylesheet" href="vue/stylesCSS/styleProf.css" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <script src="vue/scriptJS/script.js"></script>
  </head>
  <body onload="initPageTest();">
    <div id="header">
      <?php /* PROFIL */ if(isset($_SESSION['profil'])){ ?>
      <p class="profil">
      <u>Connecté en tant que</u> : 
      <b>
        <?php echo($_SESSION['profil']['prenom'] . " " . $_SESSION['profil']['nom']); ?>
      </b></p>
      <p class="profil">
        <a href="index.php?controle=user&action=deconnect">Se déconnecter</a>
      </p><?php } ?>
      <h1>QCM Live</h1>
      <p id="descr">Le site de gestion de QCM en temps réel !</p>
    </div>
    <div id="contenu">
	  <a id="accueil" href="index.php">Revenir à l'accueil</a>
      <div id="titreContenu">
        <h2>Session de test</h2>
        <h4>Test en cours : 
        <u>
          <?php echo($_SESSION['QCM_actuel']['titre_test']); ?>
        </u></h4>
      </div>
	  <div id="infos">
		<p><u>Groupe</u> : <?php echo $groupe['num_grpe']; ?>
		(<?php echo $groupe['nb_etu']?>)
		</p>
		<p><u>Thèmes</u> : 
		<?php
		for($i = 0; $i < count($themes); ++$i){
			if($i == count($themes) - 1)
				echo($themes[$i]['titre_theme'] . ".");
			else echo($themes[$i]['titre_theme'] . ", ");
		}
		?>
		</p>
	  </div>
	  <a id="masquerRep" href="javascript:void(0);"
	  onclick="showHideReponses();" value="hide">Masquer les réponses</a>
      <div class="formulaire">
        <form action="index.php?controle=prof/gestionTest&action=terminerTest" method="post">
		<?php
		$compt = 0;
		/* DEBUT FOR EACH */
		foreach($questions as $question){
			++$compt;
			$choixM = $question['bmultiple']==1?'(CM)':'';
			$balise = 'a';
			$color = 'black';
			$lien = 'index.php?controle=prof/gestionTest&action=%s&idQ=';
			$infos = '';
			if($question['bAutorise'] == 0){
				$color = '#FFAFAA';
				$lien = sprintf($lien, 'autoriserQ');
				$infos = 'Autoriser';
			}
			if($question['bAutorise'] == 1){
				$color = '#EAAAFF';
				$lien = sprintf($lien, 'bloquerQ');
				$infos = 'Bloquer';
			}
			if($question['bBloque'] == 1){
				$color = '#E4E4E4';
				$lien = '';
				$balise = 'span';
				$infos = 'Bloquée';
			}
			?>
            <fieldset>
			  <legend>Question <?php echo($compt . " " . $choixM); ?></legend>
			  <!-- BOUTON -->	
			  <<?php echo $balise; ?> style="color:<?php echo($color); ?>" 
			  title=<?php echo $infos;?> class="icone" href="
			  <?php echo($lien . $question['id_quest']); ?>">
			  <i class="fa fa-check-circle-o fa-3x"></i>
			  </<?php echo $balise; ?>>
			  <!-- TITRE -->
			  <p class="titreQuestion"><?php echo((utf8_encode($question['titre']))); ?></p>
			  <!-- REPONSES -->
			  <?php if($question['bAutorise'] == 1){ ?>
			  <div class="reponses">
			    <p>Reponses</p> :
			    <?php foreach($question['reponses'] as $reponse){
					$valide = $reponse['bvalide'] == 1 ? true : false;
					$color = 'black';
					if($valide) $color = "#9DE975";
					else $color = "#F4C76D";
					if($question['bBloque'] == 1) $color = '#E4E4E4';
				?>
				<span class="fa fa-check-circle-o fa-2x" style="color:<?php echo($color); ?>"
				title="<?php echo utf8_encode($reponse['texte_rep']); ?>">
				  <p><?php echo $reponse['nbRep']; ?></p>
			    </span>
				<?php } ?>
			  </div>
			  <?php } ?>
            </fieldset>
			<?php } ?>
          <!-- FIN FOR EACH -->
          <div class="submit">
            <input type="submit" value="Terminer le QCM" />
          </div>
        </form>
      </div>
    </div>
    <div id="footer">
      <p id="creators">Site by Thomas FOULON, Guillaume HEU &amp; Adam POUX</p>
      <img src="vue/stylesCSS/images/quiz.png" />
    </div>
  </body>
</html>
