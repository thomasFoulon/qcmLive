<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8" />
    <title>QCM Live - Resultats</title>
    <link rel="stylesheet" href="vue/stylesCSS/style.css" />
    <link rel="stylesheet" href="vue/stylesCSS/styleProf.css" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <script src="vue/scriptJS/script.js"></script>
  </head>
  <body>
    <div id="header">
      <?php /* PROFIL */ if(isset($_SESSION['profil'])){ ?>
      <p class="profil">
      <u>Connecté en tant que</u> : 
      <b>
        <?php echo($_SESSION['profil']['prenom'] . " " . $_SESSION['profil']['nom']); ?>
      </b></p>
      <p class="profil">
        <a href="index.php?controle=user&action=deconnect">Se déconnecter</a>
      </p><?php } ?>
      <h1>QCM Live</h1>
      <p id="descr">Le site de gestion de QCM en temps réel !</p>
    </div>
    <div id="contenu">
	  <a id="accueil" href="index.php">Revenir à l'accueil</a>
      <div id="titreContenu">
        <h2>Analyse des résultats</h2>
        <h4>Résultats du test : 
        <u>
          <?php echo($_SESSION['QCM_actuel']['titre_test']); ?>
        </u></h4>
      </div>
	  <div id="infos">
		<p><u>Groupe</u> : <?php echo $groupe['num_grpe']; ?>
		(<?php echo count($etudiants); ?>)
		</p>
		<p><u>Thèmes</u> : 
		<?php
		for($i = 0; $i < count($themes); ++$i){
			if($i == count($themes) - 1)
				echo($themes[$i]['titre_theme'] . ".");
			else echo($themes[$i]['titre_theme'] . ", ");
		}
		?>
		</p>
	  </div>
      <div id="tableauRes">
		<table>
		  <tr id="attributsTab">
			<th>Etudiants</th>
			<th>Note</th>
			<?php for($i = 0; $i < count($questions); ++$i){ ?>
			<th title="<?php $questions[$i]['titre']?>">Question <?php echo($i + 1); ?></th>
			<?php } ?>
		  </tr>
		<?php
		/* DEBUT FOR EACH */
		$sommeNotes = 0;
		foreach($etudiants as $etudiant){
			$sommeNotes += $etudiant['note_test'];
			if(isset($_SESSION['profil']['id_etu']) AND $etudiant['id_etu'] != $_SESSION['profil']['id_etu']) continue;
		?>
		<tr class="elemsTab">
			<th><?php echo(utf8_encode($etudiant['prenom']) . " " . strtoupper(utf8_encode($etudiant['nom']))); ?></th>
			<td><?php if(!$etudiant['absent']){echo($etudiant['note_test']);} else{echo 'abs';} ?></td>
			<?php foreach($questions as $question){ ?>
			<td>
			  <?php
				if($etudiant['absent'] == true) continue;
				foreach($question['reponses']['all'] as $reponse){
					$choisie = in_array($reponse['id_rep'], $etudiant['reponses'][$question['id_quest']]);
					$color = 'black';
					if($choisie){
						if($reponse['bvalide'] == 1)
							$color = "#9DE975";
						else $color = "#F4C76D";
					}
					else $color = "#E4E4E4";
				?>
				<div class="reponses">
				<span class="fa fa-check-circle-o fa-2x" style="color:<?php echo($color); ?>"
				title="<?php echo utf8_encode($reponse['texte_rep']); ?>">
			    </span>
				</div>
			  <?php } ?>
			</td>
			<?php } ?>
		</tr>
		<?php } /* FIN FOR EACH */ ?>
		<tr class="elemsTab">
			<th>Moyenne des étudiants</th>
			<td><?php $moy = $sommeNotes / count($etudiants); echo(round($moy, 2)); ?></td>
			<?php foreach($questions as $question){ ?>
			<td>
			  <?php foreach($question['reponses']['all'] as $reponse){
					$valide = $reponse['bvalide'] == 1 ? true : false;
					$color = 'black';
					if($valide) $color = "#9DE975";
					else $color = "#F4C76D";
				?>
				<div class="reponses">
					<span class="fa fa-check-circle-o fa-2x" style="color:<?php echo($color); ?>"
					title="<?php echo utf8_encode($reponse['texte_rep']); ?>">
					  <p><?php echo $reponse['nbRep']; ?></p>
					</span>
				</div>
			  <?php } ?>
			</td>
			<?php } ?>
		</tr>
		</table>
      </div>
    </div>
    <div id="footer">
      <p id="creators">Site by Thomas FOULON, Guillaume HEU &amp; Adam POUX</p>
      <img src="vue/stylesCSS/images/quiz.png" />
    </div>
  </body>
</html>
