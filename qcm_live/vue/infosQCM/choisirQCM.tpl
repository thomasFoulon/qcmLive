<!doctype html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
	<title>QCM Live</title>
	<link rel="stylesheet" href="vue/stylesCSS/style.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"> 
  </head>
  <body>
    <div id="header">
	  <?php /* PROFIL */ if(isset($_SESSION['profil'])){ ?>
	  <p class="profil"><u>Connecté en tant que</u> :
		<b><?php echo($_SESSION['profil']['prenom'] . " " . $_SESSION['profil']['nom']); ?></b>
	  </p>
	  <p class="profil"><a href="index.php?controle=user&action=deconnect">Se déconnecter</a></p>
	  <?php } ?>
	  <h1>QCM Live</h1>
	  <p id="descr">Le site de gestion de QCM en temps réel !</p>
	</div>
	<div id="contenu">
	  <a id="accueil" href="index.php">Revenir à l'accueil</a>
	  <div id="titreContenu">
	    <h2>Choix d'un QCM</h2>
		<h4>Choisir un QCM disponible</h4>
	  </div>
	  <div class="formulaire">
		<form action="index.php?controle=infosQCM&action=choisirQCM" method="post">
		  <fieldset>
		    <legend>QCM disponibles</legend>
			<?php if(isset($_SESSION['profil']['id_prof'])){ ?>
			<div class="elem">
			  <label>Nouveaux QCMs :</label>
			  <select name="qcmChoisi0">
				<?php foreach($QCMs as $QCM){
						if($QCM['etat'] == 0){ ?>
			    <option value="<?php echo($QCM['id_test']); ?>">
				  <?php echo($QCM['titre_test']); ?>
				</option>
				<?php } } ?>
			  </select>
			  <button type="submit" formaction="index.php?controle=infosQCM&action=choisirQCM&etat=0">
			    Choisir ce QCM
			  </button>
			</div>
			<?php } ?>
			<div class="elem">
			  <label>QCMs en cours <?php echo(isset($_SESSION['profil']['id_etu'])?'pour votre groupe':''); ?> :</label>
			  <select name="qcmChoisi1">
				<?php foreach($QCMs as $QCM){
						if($QCM['etat'] == 1){ ?>
			    <option value="<?php echo($QCM['id_test']); ?>">
				  <?php echo($QCM['titre_test']); ?>
				</option>
				<?php } } ?>
			  </select>
			  <button type="submit" formaction="index.php?controle=infosQCM&action=choisirQCM&etat=1">
			    Choisir ce QCM
			  </button>
			</div>
			<div class="elem">
			  <label>QCMs terminés :</label>
			  <select name="qcmChoisi2">
				<?php foreach($QCMs as $QCM){
						if($QCM['etat'] == 2){ ?>
			    <option value="<?php echo($QCM['id_test']); ?>">
				  <?php echo($QCM['titre_test']); ?>
				</option>
				<?php } } ?>
			  </select>
			  <button type="submit" formaction="index.php?controle=infosQCM&action=choisirQCM&etat=2">
			    Choisir ce QCM
			  </button>
			</div>
		  </fieldset>
		</form>
	  </div>
	</div>
	<div id="footer">
	  <p id="creators">Site by Thomas FOULON, Guillaume HEU &amp; Adam POUX</p>
	  <img src="vue/stylesCSS/images/quiz.png">
	</div>
</body>
</html>