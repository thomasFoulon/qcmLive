<!doctype html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
	<title>QCM Live</title>
	<link rel="stylesheet" href="vue/stylesCSS/style.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"> 
	<script src="vue/scriptJS/script.js"></script>
  </head>
  <body>
    <div id="header">
	  <?php /* PROFIL */ if(isset($_SESSION['profil'])){ ?>
	  <p class="profil"><u>Connecté en tant que</u> :
		<b><?php echo($_SESSION['profil']['prenom'] . " " . $_SESSION['profil']['nom']); ?></b>
	  </p>
	  <p class="profil"><a href="index.php?controle=user&action=deconnect">Se déconnecter</a></p>
	  <?php } ?>
	  <h1>QCM Live</h1>
	  <p id="descr">Le site de gestion de QCM en temps réel !</p>
	</div>
	<div id="contenu">
	  <a id="accueil" href="index.php">Revenir à l'accueil</a>
	  <div id="titreContenu">
	    <h2>Créer un QCM</h2>
		<h4>Création d'un QCM pour un groupe</h4>
	  </div>
	  <div class="formulaire">
		<form action="index.php?controle=prof/editQCM&action=creerQCM" method="post">
		  <fieldset>
		    <legend>Informations du QCM</legend>
			<div class="elem">
			  <label>Titre du QCM :</label>
			  <input onfocus="delMsg()" name="titre" type="text" value="<?php echo($titreTest); ?>"/>
			</div>
			<div class="elem">
			  <label>Groupe :</label>
			  <select onfocus="delMsg()" name="groupe">
			    <?php
					foreach($groupes as $groupe){
						$selected = $groupe['id_grpe']==$id_groupe?'selected':'';
						echo("<option " . $selected .
						" value=" . $groupe['id_grpe'] .">".
						$groupe['num_grpe'] . "</option>");
					}
			    ?>
			  </select>
			</div>
		  </fieldset>
		  <div class="submit">
		    <input type="submit" value="Créer le QCM"/>
		  </div>
		</form>
		<p id="msgErr" ><?php echo($msg); ?></p>
	  </div>
	</div>
	<div id="footer">
	  <p id="creators">Site by Thomas FOULON, Guillaume HEU &amp; Adam POUX</p>
	  <img src="vue/stylesCSS/images/quiz.png">
	</div>
</body>
</html>