<!doctype html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
	<title>QCM Live</title>
	<link rel="stylesheet" href="vue/stylesCSS/style.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="vue/scriptJS/script.js"></script>
  </head>
  <body>
    <div id="header">
	  <?php /* PROFIL */ if(isset($_SESSION['profil'])){ ?>
	  <p class="profil"><u>Connecté en tant que</u> :
		<b><?php echo($_SESSION['profil']['prenom'] . " " . $_SESSION['profil']['nom']); ?></b>
	  </p>
	  <p class="profil"><a href="index.php?controle=user&action=deconnect">Se déconnecter</a></p>
	  <?php } ?>
	  <h1>QCM Live</h1>
	  <p id="descr">Le site de gestion de QCM en temps réel !</p>
	</div>
	<div id="contenu">
	  <a id="accueil" href="index.php">Revenir à l'accueil</a>
	  <div id="titreContenu">
	    <h2>Modifier un QCM</h2>
		<h4>Modification de <u><?php echo($_SESSION['QCM_actuel']['titre_test']); ?></u></h4>
	  </div>
	  <div class="formulaire">
		<form action="index.php?controle=prof/editQCM&action=modifQCM" method="post">
		  <fieldset>
		    <legend>Ajouter une question</legend>
			<div class="elem">
			  <label><b>Titre de la question</b> :</label>
			  <input onclick="delMsg();" type="text" name="titre"></input>
			</div>
			<div class="elem">
			  <label>Thème :</label>
			  <select onclick="delMsg();" id="theme" name="theme" onchange="showHideNewTheme();">
			    <option value="-1">Nouveau thème</option>
				<?php foreach($themes as $theme){ ?>
				<option value="<?php echo $theme['id_theme']; ?>">
				<?php echo $theme['titre_theme']; ?>
				</option>
				<?php } ?>
			  </select>
			  <input onclick="delMsg();" id="newTheme" type="text" name="newTheme" placeholder="Nouveau thème">
			</div>
			<fieldset id="add">
			  <legend>Réponses</legend>
			  <div class="elemRep">
			    <label>Réponse 1 :</label>
			    <input onclick="delMsg();" class="rep" type="text" name="reponses[0]">
			    <input onclick="delMsg();" type="checkbox" name="valides[0]">Correcte
			  </div>
			  <div class="elemRep">
			    <label>Réponse 2 :</label>
			    <input onclick="delMsg();" class="rep" type="text" name="reponses[1]">
			    <input onclick="delMsg();" type="checkbox" name="valides[1]">Correcte
			  </div>
			  <input type="button" value="Ajouter une réponse" onclick="addReponse(this, 1);">
		    </fieldset>
			<div class="submit">
		      <input type="submit" value="Ajouter la question"/>
		    </div>
			<?php if(isset($msg)){ ?><p id="msgErr"><?php echo $msg; ?></p><?php } ?>
		  </fieldset>
		  <fieldset>
		    <legend>Questions du QCM</legend>
		    <?php
				if(count($questions) == 0){
					echo("<p class=\"question\">Aucune question ...</p>");
				}
				else{
					$compt = 0;
					foreach($questions as $question){
			?>
			  <a class="question"
			  href="index.php?controle=prof/editQCM&action=modifQCM&idQ=<?php echo $question['id_quest'];?>"
			  title="Supprimer la question <?php echo ++$compt; ?>">
				<p class="titreQ"><u>Question <?php echo $compt; ?></u> : <?php echo utf8_encode($question['titre']);?></p>
				<?php
				  foreach($question['reponses'] as $reponse){
					$color = $reponse['bvalide']==1?'#9DE975':'#F4C76D';
				?>
			    <div class="reponse">
				  <span class="fa fa-check-circle-o fa" style="color:<?php echo($color); ?>"></span>
				  <p><?php echo(utf8_encode($reponse['texte_rep'])); ?></p>
			    </div>
			  <?php } ?>
			  </a>
			  <?php } } ?>
		  </fieldset>
		</form>
	  </div>
	</div>
	<div id="footer">
	  <p id="creators">Site by Thomas FOULON, Guillaume HEU &amp; Adam POUX</p>
	  <img src="vue/stylesCSS/images/quiz.png">
	</div>
</body>
</html>