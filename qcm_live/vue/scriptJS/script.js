/* afficherTest.tpl (PROF) */

function changeButtonRep(){
	var elem = document.getElementById("masquerRep");
	if(elem.value == "hide"){
		elem.value = "show";
		elem.innerHTML = "Voir les réponses";
	}
	else{
		elem.value = "hide";
		elem.innerHTML = "Masquer les réponses";
	}
}

function initPageTest(){
	changeButtonRep();
}

function showHideReponses()
{
	var but = document.getElementById("masquerRep");
	var elems = document.getElementsByClassName("reponses");
	var value = "inline";
	if(but.value == "hide")
		value = "none";
	for (i = 0; i < elems.length; i++)
		elems[i].style.display = value;
	changeButtonRep();
}

/* afficherTest.tpl (ETU) */

function uncheckRep(reponse){
	var elems = reponse.getElementsByTagName("input");
	var icones = reponse.getElementsByTagName("span");
	if(elems[0].checked == true){
		elems[0].checked = false;
		icones[0].style.color = "#FFAFAA";
	}
}

function checkRep(reponse, choixMultiple){
	var parentRep = reponse.parentNode;
	var elems = reponse.getElementsByTagName("input");
	var icones = reponse.getElementsByTagName("span");
	if(elems[0].checked == false){
		elems[0].checked = true;
		icones[0].style.color = "#EAAAFF";
		if(choixMultiple == 0){ // Désélectionner les autres
			var allElems = parentRep.getElementsByClassName("reponseQ");
			Array.prototype.forEach.call(allElems, function(elem){
				if(elem == reponse) return;
				uncheckRep(elem);
			});
		}
	}
	else{
		elems[0].checked = false;
		icones[0].style.color = "#FFAFAA";
	}
}

/* ident.tpl */

function delMsg()
{
	var elem = document.getElementById("msgErr");
	elem.innerHTML = "";
}

/* modifQCM.tpl */

function addReponse(buttonAdd, position){
	var elem = document.getElementById("add");
	elem.removeChild(buttonAdd);
	position = parseInt(position);
	++position;
	var content = "<div class=\"elemRep\"><label>Réponse " + (position + 1) + " :</label><input onclick=\"delMsg();\" class=\"rep\" type=\"text\" name=\"reponses[" + position + "]\"><input type=\"checkbox\" name=\"valides[" + position + "]\">Correcte</div><input type=\"button\" value=\"Ajouter une réponse\" onclick=\"addReponse(this, " + position + ");\">";
	$('#add').append(content);
}

function showHideNewTheme(){
	var elem = document.getElementById("theme");
	var elemNewTheme = document.getElementById("newTheme");
	if(elem.value == "-1")
		elemNewTheme.disabled = false;
	else elemNewTheme.disabled = true;
}