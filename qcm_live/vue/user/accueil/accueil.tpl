<!doctype html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
	<title>QCM Live - Accueil</title>
	<link rel="stylesheet" href="vue/stylesCSS/style.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"> 
  </head>
  <body>
    <div id="header">
	  <h1>QCM Live</h1>
	  <p id="descr">Le site de gestion de QCM en temps réel !</p>
	</div>
	<div id="contenu">
	  <div id="titreContenu">
	    <h2>Accueil</h2>
		<h4>Sélectionnez un service</h4>
	  </div>
	  <div id="selectService">
	    <a href="index.php?controle=user&action=identEtu">Accéder à l'espace étudiant</a>
		<a href="index.php?controle=user&action=identProf">Accéder à l'espace professeur</a>
	  </div>
	</div>
	<div id="footer">
	  <p id="creators">Site by Thomas FOULON, Guillaume HEU &amp; Adam POUX</p>
	  <img src="vue/stylesCSS/images/quiz.png">
	</div>
</body>
</html>