<!doctype html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
	<title>QCM Live - Etudiant</title>
	<link rel="stylesheet" href="vue/stylesCSS/style.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"> 
  </head>
  <body>
    <div id="header">
	  <?php /* PROFIL */ if(isset($_SESSION['profil'])){ ?>
	  <p class="profil"><u>Connecté en tant que</u> :
		<b><?php echo($_SESSION['profil']['prenom'] . " " . $_SESSION['profil']['nom']); ?></b>
	  </p>
	  <p class="profil"><a href="index.php?controle=user&action=deconnect">Se déconnecter</a></p>
	  <?php } ?>
	  <h1>QCM Live</h1>
	  <p id="descr">Le site de gestion de QCM en temps réel !</p>
	</div>
	<div id="contenu">
	  <div id="titreContenu">
	    <h2>Accueil étudiant</h2>
		<h4>Sélectionnez un service</h4>
	  </div>
	  <div id="selectService">
		<a href="index.php?controle=infosQCM&action=choisirQCM">
		  Sélectionner un <?php echo(isset($_SESSION['QCM_actuel'])?'autre':''); ?> QCM
		</a>
		<?php if(isset($_SESSION['QCM_actuel'])){ ?>
		<hr class="separator">
		<p id="qcmActuel">Actions liées à <b><?php echo($_SESSION['QCM_actuel']['titre_test']);?></b></p>
		<?php if($_SESSION['QCM_actuel']['etat'] == '1'){ ?>
		<a href="index.php?controle=etu/effectuerTest&action=commencerTest">Participer à un test avec ce QCM</a>
		<?php } ?>
		<?php if($_SESSION['QCM_actuel']['etat'] == '2'){ ?>
		<a href="index.php?controle=infosQCM&action=analyseResultats">Voir vos résultats pour ce QCM</a>
		<?php } ?>
		<?php } ?>
	  </div>
	</div>
	<div id="footer">
	  <p id="creators">Site by Thomas FOULON, Guillaume HEU &amp; Adam POUX</p>
	  <img src="vue/stylesCSS/images/quiz.png">
	</div>
</body>
</html>