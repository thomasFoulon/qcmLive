<!doctype html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
	<title>QCM Live - Erreur</title>
	<link rel="stylesheet" href="vue/stylesCSS/style.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"> 
  </head>
  <body>
    <div id="header">
	  <?php /* PROFIL */ if(isset($_SESSION['profil'])){ ?>
	  <p class="profil">Connecté en tant que : <?php echo($_SESSION['profil']['nom']); ?></p>
	  <p class="profil"><a href="index.php?controle=user&action=deconnect">Se déconnecter</a></p>
	  <?php } ?>
	  <h1>QCM Live</h1>
	  <p id="descr">Le site de gestion de QCM en temps réel !</p>
	</div>
	<div id="contenu">
	  <div id="titreContenu">
	    <h2>ERREUR</h2>
		<h4><?php echo(isset($erreur)?$erreur:''); ?></h4>
	  </div>
	  <div id="selectService">
	    <a href="index.php">Revenir à l'accueil</a>
	  </div>
	</div>
	<div id="footer">
	  <p id="creators">Site by Thomas FOULON, Guillaume HEU &amp; Adam POUX</p>
	</div>
</body>
</html>