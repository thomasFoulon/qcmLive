<!doctype html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
	<title>QCM Live - Identification</title>
	<link rel="stylesheet" href="vue/stylesCSS/style.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<script type="text/javascript" src="vue/scriptJS/script.js"></script>
  </head>
  <body>
    <div id="header">
	  <h1>QCM Live</h1>
	  <p id="descr">Le site de gestion de QCM en temps réel !</p>
	</div>
	<div id="contenu">
	  <a id="accueil" href="index.php">Revenir à l'accueil</a>
	  <div id="titreContenu">
	    <h2>Identification</h2>
		<h4>Se connecter en tant <?php echo($type=='Prof'?'que professeur':'qu\'étudiant'); ?></h4>
	  </div>
	  <div class="formulaire">
		<form action="index.php?controle=user&action=ident<?php echo(isset($type)?$type:''); ?>" method="post">
		  <fieldset>
		    <legend>Informations nécessaires</legend>
			<div class="elem">
			  <label>Login :</label>
			  <input name="login" type="text" onfocus="delMsg();" value="<?php echo($login) ?>"/>
			</div>
			<div class="elem">
			  <label>Password :</label>
			  <input name="password" type="password" onfocus="delMsg();" value="<?php echo($password) ?>"/>
			</div>
		  </fieldset>
		  <div class="submit">
		    <input type="submit" value="Se connecter"/>
		  </div>
		</form>
		<p id="msgErr"><?php echo $msg; ?></p>
	  </div>
	</div>
	<div id="footer">
	  <p id="creators">Site by Thomas FOULON, Guillaume HEU &amp; Adam POUX</p>
	  <img src="vue/stylesCSS/images/quiz.png">
	</div>
</body>
</html>